package tdd._02;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import tdd._01.SemVer;

/**
 * <p>[概 要] SemVerServiceを試験するテストクラス。</p>
 * <p>[詳 細] </p>
 * <p>[備 考] </p>
 * <p>[環 境] JRE 8</p>
 *
 * @author t.nemoto.x
 */
@RunWith(Enclosed.class)
public class SemVerServiceTest {

  public static class バージョン情報の比較を行う {

    private SemVerService semVerService;

    @Before
    public void setUp() {
      semVerService = new SemVerService();
    }

    @Test
    public void バージョンが等しいオブジェクトを比較してtrueとなることを確認する() {
      boolean actual = semVerService.compareEquals(new SemVer(1, 4, 2), new SemVer(1, 4, 2));
      assertThat(actual, is(true));
    }

    @Test
    public void バージョンが等しくないオブジェクトを比較してfalseとなることを確認する() {
      boolean actual = semVerService.compareEquals(new SemVer(1, 4, 2), new SemVer(2, 1, 0));
      assertThat(actual, is(false));
    }
  }
  
  public static class バージョンをあげる {

    private SemVerService semVerService;

    @Before
    public void setUp() {
      semVerService = new SemVerService();
    }

    @Test
    public void patchがインクリメントされることを確認する() {
      SemVer actual = semVerService.bugfix(new SemVer(1, 0, 0));
      assertThat(actual.toString(), is("1.0.1"));
    }

    @Test
    public void minorがインクリメントされることを確認する() {
      SemVer actual = semVerService.minorVersionUp(new SemVer(1, 0, 1));
      assertThat(actual.toString(), is("1.1.0"));
    }
    
    @Test
    public void majorがインクリメントされることを確認する() {
      SemVer actual = semVerService.majorVersionUp(new SemVer(1, 2, 1));
      assertThat(actual.toString(), is("2.0.0"));
    }
    
    @Test
    public void bugfixしたときに返却されるオブジェクトが別のインスタンスであることを確認する() {
      SemVer args = new SemVer(1, 0, 0);
      SemVer actual = semVerService.bugfix(args);
      assertFalse(actual == args);
    }
    
    @Test
    public void bugfixしたときに引数で渡したインスタンスの値は変わっていない() {
      SemVer args = new SemVer(1, 0, 0);
      semVerService.bugfix(args);
      assertThat(args.toString(), is("1.0.0"));
    }
    
    @Test
    public void マイナーバージョンアップ時に返却されるオブジェクトが別のインスタンスであることを確認する() {
      SemVer args = new SemVer(1, 0, 1);
      SemVer actual = semVerService.minorVersionUp(args);
      assertFalse(actual == args);
    }

    @Test
    public void マイナーバージョンアップ時に引数で渡したインスタンスの値は変わっていない() {
      SemVer args = new SemVer(1, 0, 1);
      semVerService.minorVersionUp(args);
      assertThat(args.toString(), is("1.0.1"));
    }

    @Test
    public void メジャーバージョンアップ時に返却されるオブジェクトが別のインスタンスであることを確認する() {
      SemVer args = new SemVer(1, 1, 1);
      SemVer actual = semVerService.majorVersionUp(args);
      assertFalse(actual == args);
    }

    @Test
    public void メジャーバージョンアップ時に引数で渡したインスタンスの値は変わっていない() {
      SemVer args = new SemVer(1, 1, 1);
      semVerService.majorVersionUp(args);
      assertThat(args.toString(), is("1.1.1"));
    }
    
  }
}
