package tdd._01;

/**
 * <p>[概 要] セマンティックバージョンを保持するモデル。</p>
 * <p>[詳 細] </p>
 * <p>[備 考] </p>
 * <p>[環 境] JRE 8</p>
 *
 * @author t.nemoto.x
 */
public class SemVer {

  /**
   * <p>[概 要] コピーコンストラクタ。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   *
   * @param semVer コピー元バージョン情報
   */
  public SemVer(SemVer semVer) {
    setMajor(semVer.getMajor());
    setMinor(semVer.getMinor());
    setPatch(semVer.getPatch());
  }

  /**
   * <p>[概 要] コンストラクタ。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   *
   * @param major メジャー
   * @param minor マイナー
   * @param patch パッチ
   */
  public SemVer(int major, int minor, int patch) {
    setMajor(major);
    setMinor(minor);
    setPatch(patch);
  }

  /**
   * <p>[概 要] コンストラクタ。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   *
   */
  public SemVer() {
    // 何もしない
  }

  /**
   * メジャー
   */
  private int major;
  
  /**
   * マイナー
   */
  private int minor;

  /**
   * パッチ
   */
  private int patch;

  /**
   * <p>[概 要] メジャー を取得する。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   *
   * @return メジャー
   */
  public int getMajor() {
    return major;
  }

  /**
   * <p>[概 要] メジャー を設定する。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   *
   * @param major メジャー
   */
  public void setMajor(int major) {
    if (isNegative(major)) {
      throw new IllegalArgumentException(major + " is negative!!");
    }
    this.major = major;
  }

  /**
   * <p>[概 要] マイナー を取得する。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   *
   * @return マイナー
   */
  public int getMinor() {
    return minor;
  }

  /**
   * <p>[概 要] マイナー を設定する。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   *
   * @param minor マイナー
   */
  public void setMinor(int minor) {
    if (isNegative(minor)) {
      throw new IllegalArgumentException(minor + " is negative!!");
    }

    this.minor = minor;
  }

  /**
   * <p>[概 要] パッチ を取得する。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   *
   * @return パッチ
   */
  public int getPatch() {
    return patch;
  }

  /**
   * <p>[概 要] パッチ を設定する。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   *
   * @param patch パッチ
   */
  public void setPatch(int patch) {
    if (isNegative(patch)) {
      throw new IllegalArgumentException(patch + " is negative!!");
    }

    this.patch = patch;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + major;
    result = prime * result + minor;
    result = prime * result + patch;
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    SemVer other = (SemVer) obj;
    if (major != other.major)
      return false;
    if (minor != other.minor)
      return false;
    if (patch != other.patch)
      return false;
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return String.join(".", String.valueOf(major), String.valueOf(minor), String.valueOf(patch));
  }
  
  /**
   * <p>[概 要] パッチを0にする。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   */
  public void resetPatch() {
    patch = 0;
  }
  
  /**
   * <p>[概 要] マイナーを0にする。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   */
  public void resetMinor() {
    minor = 0;
  }  
  
  /**
   * <p>[概 要] パッチを1増やす。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   *
   */
  public void addPatch() {
    patch++;
  }
  
  /**
   * <p>[概 要] マイナーを1増やす。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   */
  public void addMinor() {
    minor++;
  }

  /**
   * <p>[概 要] メジャーを1増やす。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   */
  public void addMajor() {
    major++;
  }

  /**
   * <p>[概 要] 負数の判定をする。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   *
   * @param num 判定する数値
   * @return 負数の場合はtrue
   */
  private boolean isNegative(int num) {
    if (num < 0) {
      return true;
    }
    return false;
  }
}
