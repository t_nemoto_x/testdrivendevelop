package tdd._02;

import tdd._01.SemVer;

public class SemVerService {

  /**
   * <p>[概 要] 引数で渡されたバージョンが等しいかどうかを確認する。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   *
   * @param sem1 バージョンオブジェクト1
   * @param sem2 バージョンオブジェクト2
   * @return 比較結果
   */
  public boolean compareEquals(SemVer sem1, SemVer sem2) {
    return sem1.equals(sem2);
  }

  /**
   * <p>[概 要] バグ修正時にパッチの番号を1つあげる。</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   *
   * @param semVer バージョン情報
   * @return バージョン情報
   */
  public SemVer bugfix(SemVer semVer) {
    SemVer newSemVer = new SemVer(semVer);
    newSemVer.addPatch();
    return newSemVer;
  }

  /**
   * <p>[概 要] マイナーバージョンアップ時にマイナー番号を一つあげる</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   *
   * @param semVer バージョン情報
   * @return バージョン情報
   */
  public SemVer minorVersionUp(SemVer semVer) {
    SemVer newSemVer = new SemVer(semVer);
    newSemVer.addMinor();
    newSemVer.resetPatch();
    return newSemVer;
  }

  /**
   * <p>[概 要] メジャーバージョンアップ時にマイナー番号を一つあげる</p>
   * <p>[詳 細] </p>
   * <p>[備 考] </p>
   *
   * @param semVer バージョン情報
   * @return バージョン情報
   */
  public SemVer majorVersionUp(SemVer semVer) {
    SemVer newSemVer = new SemVer(semVer);
    newSemVer.addMajor();
    newSemVer.resetMinor();
    newSemVer.resetPatch();
    return newSemVer;
  }
}
