package tdd._01;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

@RunWith(Enclosed.class)
public class SemVerTest {

  // いらないから消すけど、レビュー時に思考の過程を説明するためにコメントアウト
  //
  // public static class バージョンオブジェクトを作成する {
  //
  // @Test
  // public void バージョンの中身が空の作成されることを確認する() {
  // assertThat(new SemVer(), is(new SemVer()));
  // }
  //
  // @Test
  // public void バージョンの中身が1_4_2であるオブジェクトが作成されることを確認する() {
  // SemVer actual = new SemVer();
  // actual.setMajor(1);
  // actual.setMinor(4);
  // actual.setPatch(2);
  // assertThat(new SemVer(1, 4, 2), is(actual));
  // }
  // }

  public static class バージョンオブジェクトの文字列表現を取得する {
    @Test
    public void バージョン1_4_2の文字列表現を取得する() {
      assertThat(new SemVer(1, 4, 2).toString(), is("1.4.2"));
    }

    @Test
    public void バージョン1_4_3の文字列表現を取得する() {
      assertThat(new SemVer(1, 4, 3).toString(), is("1.4.3"));
    }
  }

  public static class 負数と境界値のチェック {
    // いらないから消すけど、レビュー時に思考の過程を説明するためにコメントアウト
    //
    // @Test(expected=IllegalArgumentException.class)
    // public void setMajorに負数が与えられた時例外が発生することを確認する() {
    // SemVer semVer = new SemVer();
    // semVer.setMajor(-1);
    // }
    //
    // @Test(expected=IllegalArgumentException.class)
    // public void setMinorに負数が与えられた時例外が発生することを確認する() {
    // SemVer semVer = new SemVer();
    // semVer.setMinor(-1);
    // }
    //
    // @Test(expected=IllegalArgumentException.class)
    // public void setPatchに負数が与えられた時例外が発生することを確認する() {
    // SemVer semVer = new SemVer();
    // semVer.setPatch(-1);
    // }

    @Test(expected = IllegalArgumentException.class)
    public void コンストラクタでmajorに負数が与えられた時例外が発生することを確認する() {
      new SemVer(-1, 0, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void コンストラクタでminorに負数が与えられた時例外が発生することを確認する() {
      new SemVer(0, -1, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void コンストラクタでpatchに負数が与えられた時例外が発生することを確認する() {
      new SemVer(0, 0, -1);
    }
  }
}
